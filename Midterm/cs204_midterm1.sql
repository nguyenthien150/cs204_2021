-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Aug 21, 2021 at 01:25 PM
-- Server version: 10.4.20-MariaDB
-- PHP Version: 8.0.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `cs204_midterm1`
--

-- --------------------------------------------------------

--
-- Table structure for table `planets`
--

CREATE TABLE `planets` (
  `ID` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `moons` int(11) NOT NULL,
  `position` int(11) NOT NULL,
  `type` varchar(255) NOT NULL,
  `imgurl` varchar(255) NOT NULL,
  `date_created` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `planets`
--

INSERT INTO `planets` (`ID`, `name`, `moons`, `position`, `type`, `imgurl`, `date_created`) VALUES
(1, 'Mercury', 0, 1, 'terrestrial', 'https://upload.wikimedia.org/wikipedia/commons/thumb/d/d9/Mercury_in_color_-_Prockter07-edit1.jpg/1200px-Mercury_in_color_-_Prockter07-edit1.jpg', '2021-08-21 10:57:53'),
(2, 'Venus', 0, 2, 'terrestrial', 'http://cen.acs.org/content/dam/cen/99/11/WEB/09911-feature3-venus.jpg', '2021-08-21 10:58:28'),
(3, 'Earth', 1, 3, 'terrestrial', 'https://planetfacts.org/wp-content/uploads/2010/06/Earth.jpg', '2021-08-21 10:59:39'),
(4, 'Mars', 2, 4, 'terrestrial', 'https://evaspellman.files.wordpress.com/2013/06/images.jpg', '2021-08-21 11:00:31'),
(5, 'Jupiter', 79, 5, 'gaseous', 'https://upload.wikimedia.org/wikipedia/commons/2/2b/Jupiter_and_its_shrunken_Great_Red_Spot.jpg', '2021-08-21 11:01:22'),
(6, 'Saturn', 82, 6, 'gaseous', 'https://scitechdaily.com/images/Saturn-Season-Transitions.gif', '2021-08-21 11:02:13'),
(7, 'Uranus', 27, 27, 'gaseous', 'https://i.pinimg.com/originals/50/92/3e/50923e1764ad4fdc3c6ff32e2aaa122d.gif', '2021-08-21 11:02:47'),
(8, 'Neptune', 17, 8, 'gaseous', 'https://i.pinimg.com/originals/15/0e/57/150e571009d45c5ea290ec66fc2bdb49.jpg', '2021-08-21 11:03:26');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `planets`
--
ALTER TABLE `planets`
  ADD PRIMARY KEY (`ID`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `planets`
--
ALTER TABLE `planets`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
