# Task
Create a small webstie about the planets of the Solar System. You will need to add the ability to create new planets and display them individually on a 'planet.php' page and all of the planets on the home page. Upload the database provided by the lecturer to your server and use the database name specified below.
# Pages to create
- planet.php: Displays individual planets based on the requested ID
- create.php: Form to create / add a planet
# Database
- Upload the .sql file provided by the lecturer to a database named "cs204_midterm1"
- Use this same name in your database connection file
# Form
Create the following fields:
- Input (type="text", name="planet" [The planet's name])
- Input (type="number", name="moons" [The number of moons])
- Input (type="number", name="position" [Position from the Sun, i.e. Earth = 3])
- Input x2 (type="radio", name="type", value="terrestrial" & "gaseous" [Planet type])
- Input (type="text", name="imgurl" [An external URL link to the planet image from Google]) <br />
Implement some basic error handling. Use prepare statements to insert the new planet into the database and then redirect the user to the newly created planet using the header() function.
# BONUS POINTS
- Add edit and delete functions
- Create an about page, use your own design and pull the planets from the database
- Use Font Awesome icons throughout the project
